// Armamos las Rutas para  nuestro producto

const express = require('express');
const router = express.Router();
const clienteController = require('../controllers/clienteController');


// rutas CRUD

router.get('/', clienteController.mostrarClientes);
router.post('/', clienteController.crearCliente);


module.exports = router;

