const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    categoria:{
        type: String,
        required: true
    },
    nombre:{
        type: String,
        required: true
    },
    
    presentacion:{
        type: String,
        required: true
    },
    cantidad:{
        type: Number,
        required: true
    },
    precio_venta:{
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Producto', productoSchema );
