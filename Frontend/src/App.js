import React, { Fragment } from 'react';
import Login from './paginas/auth/Login';
import Home from './paginas/Home';

//componente Fradgmente como contenedor
/**Configuración básica de React Router
Una vez instalado, podemos traer nuestro primer componente 
que se requiere para usar React router, que se llama BrowserRouter.
Ten en cuenta que hay varios tipos de enrutadores que proporciona react-enrutador-dom, además de BrowserRouter, en el que no entraremos. Es una práctica común poner un alias (renombrar) BrowserRoute simplemente como 'Router' cuando se importa. */
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import CrearCuenta from './paginas/auth/CrearCuenta';
import ProductosAdmin from './paginas/productos/ProductosAdmin';
import ProductosCrear from './paginas/productos/ProductosCrear';
import ProductosEditar from './paginas/productos/ProyectosEditar';

/**  Declaramos rutas dentro del componente Router como secundarias. Podemos declarar tantas rutas como queramos y necesitamos proporcionar al menos dos propiedades para cada ruta, path y component (o render): */
function App() {
  return (
    <Fragment>
      <Router> 
        <Routes>
        <Route path="/" exact element={<Login/>}/>
        <Route path="/crear-cuenta" exact element={<CrearCuenta/>}/>
        <Route path="/home" exact element={<Home/>}/>
        <Route path="/productos-admin" exact element={<ProductosAdmin/>}/>
        <Route path="/productos-crear" exact element={<ProductosCrear/>}/>
        <Route path="/productos-editar/:idproducto" exact element={<ProductosEditar/>}/>
    
        </Routes>

      </Router>
    </Fragment>
  );
}

export default App;
